<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>PHP Objects 2</title>

    <link rel="stylesheet" type="text/css" href="css/style.css">

</head>

<body>

<form class="basic-grey" action="results.php" method="post">

    <div>
        <label for="petType">What type of Pet?</label>
        <select name="petType" id="petType">
            <option value="cat">Cat</option>
            <option value="dog">Dog</option>
        </select>
    </div>
    <div>
        <label for="petName">* Pet Name:</label>
        <input type="text" name="petName" id="petName"/>
    </div>

    <div>
        <input type="submit" class="button" value="Submit"/>
    </div>


</form>

<div class="basic-grey instructions">
    <h1>Instructions</h1>
    <h2>Part 1</h2>
    <p>
        Your Dog class declares four public properties. Create PHP getter and setter methods that allow the values of
        those properties to be set. Then change those properties to be <b>private.</b></p>

    <h2>Part 2</h2>
    <p>
        Using the provided class diagram as a guide, create an abstract class called Pet. Pet should declare four
        private properties, the getter and setter methods, and the three convenience methods that were provided to you.
    </p>

    <p>Change your Dog class to inherit from the Pet class and remove unnecessarily duplicated code.</p>

    <p>Then declare a new class called Cat that inherits from the Pet class and add in any necessary methods.</p>

    <p>Finally, use the new form presented here to allow a user to create a pet of EITHER type cat or type dog. Write
        the value of the fullDescription() method on the results.php page.</p>

</div>


</body>
</html>