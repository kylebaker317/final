<?php
include("_includes/dog.class.php");
include("_includes/cat.class.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Result: PHP Objects 1</title>

    <link rel="stylesheet" type="text/css" href="css/style.css">

</head>

<body>


<?php

// COLLECT THE VALUES FROM THE FORM
$petType = $_POST["petType"];
$petName = $_POST["petName"];

// CREATE A NEW INSTANCE OF THE CORRECT TYPE
if ($petType == "dog") {
    $myPet = new Dog($petName);
} else {
    $myPet = new Cat($petName);
}

?>

<div class="basic-grey">
    <?php echo "<h1>Here's the information about your {$petType}:</h1>"; ?>

    <p>Pet Name: <?php echo $myPet->getName(); ?></p>
    <p>Description: <?php echo $myPet->fullDescription() ?> </p>

</div>

</body>
</html>