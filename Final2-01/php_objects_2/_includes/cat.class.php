<?php
include_once("pet.class.php");

class Cat extends Pet
{
    public static function randomBreed()
    {
        // SET UP AN ARRAY OF VALUES
        $input = array("Abyssinian", "American-Bobtail", "American-Curl", "American-Shorthair", "American-Wirehair",
            "Balinese", "Bengal", "Birman", "Bombay", "British-Shorthair", "Burmese", "Chartreux", "Cornish-Rex",
            "Cymric", "Devon-Rex", "Egyptian-Mau", "Exotic-Shorthair", "Havana-Brown", "Himalayan", "Japanese-Bobtail",
            "Javanese", "Korat", "Maine-Coon", "Manx", "Munchkin", "Nebelung", "Norwegian-Forest-Cat", "Ocicat",
            "Oriental", "Persian", "Ragdoll", "Russian-Blue", "Scottish-Fold", "Selkirk-Rex", "Siamese", "Siberian",
            "Singapura", "Snowshoe", "Somali", "Sphynx", "Tonkinese", "Turkish-Angora", "Turkish-Van");
        // RETURN A SINGLE RANDOM ELEMENT FROM THE ARRAY
        return array_rand(array_flip($input), 1);
    }
}
