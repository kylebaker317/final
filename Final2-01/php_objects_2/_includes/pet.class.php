<?php

abstract class Pet
{

    private $name;
    private $descriptor;
    private $color;
    private $breed;

    function __construct($petName = '')
    {
        $this->setName($petName);
        $this->setDescriptor();
        $this->setColor();
        $this->setBreed();
    }

    // METHODS THAT NEED TO BE DEFINED BY CHILD CLASSES
    abstract public static function randomBreed();

    // NAME FUNCTIONS
    public function setName($name)
    {
        return $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    // DESCRIPTOR FUNCTIONS
    public function setDescriptor($descriptor = null)
    {
        if ($descriptor == null) {
            return $this->descriptor = $this->randomDescriptor();
        } else {
            return $this->descriptor = print_r($descriptor, true);
        }
    }

    public function getDescriptor()
    {
        return $this->descriptor;
    }

    // COLOR FUNCTIONS
    public function setColor()
    {
        return $this->color = $this->randomColor();
    }

    public function getColor()
    {
        return $this->color;
    }

    // BREED FUNCTIONS
    public function setBreed()
    {
        return $this->breed = $this->randomBreed();
    }

    public function getBreed()
    {
        return $this->breed;
    }

    // PRE-BUILT FUNCTIONS

    public static function randomColor()
    {
        // SET UP AN ARRAY OF VALUES
        $input = array("tan", "brown", "black", "white", "spotted");

        // RETURN A SINGLE RANDOM ELEMENT FROM THE ARRAY
        return array_rand(array_flip($input), 1);
    }

    public function fullDescription()
    {
        return "Your pet is a $this->descriptor $this->color $this->breed named $this->name.";
    }

    public static function randomDescriptor()
    {
        // SET UP AN ARRAY OF VALUES
        $input = array("stinky", "huge", "tiny", "lazy", "lovable");

        // RETURN A SINGLE RANDOM ELEMENT FROM THE ARRAY
        return array_rand(array_flip($input), 1);
    }
}