<?php
include_once("pet.class.php");

class Dog extends Pet
{
    public static function randomBreed()
    {
        // SET UP AN ARRAY OF VALUES
        $input = array("german shepherd", "dachsund", "retriever", "labradoodle", "bulldog");

        // RETURN A SINGLE RANDOM ELEMENT FROM THE ARRAY
        return array_rand(array_flip($input), 1);
    }
}